package d200oop.d211clazz_and_object;

public class Student {
  private String id;
  String fullName;
  double gpax;
  private String currentLevel;

  Student() {

  }

  public void register() {

  }

  public void addSubject() {

  }

  public void removeSubject() {

  }

  public void exam() {

  }

  //////////////////////////////

  public void displayStudentInfo(String fullName, String homeNumber, String street, String district, String province, String country) {

    System.out.println("Student Info 1:\n" +
        "fullName => " + fullName + "\n" +
        "homeNumber => " + homeNumber + "\n" +
        "street => " + street + "\n" +
        "district => " + district + "\n" +
        "province => " + province + "\n" +
        "country => " + country);
  }

  public void displayStudentInfo(String fullName, Address address) {
    System.out.println("Student Info 2:\n" +
        "fullName => " + fullName + "\n" +
        "homeNumber => " + address.homeNumber + "\n" +
        "street => " + address.street + "\n" +
        "district => " + address.district + "\n" +
        "province => " + address.province + "\n" +
        "country => " + address.country);
  }

}
