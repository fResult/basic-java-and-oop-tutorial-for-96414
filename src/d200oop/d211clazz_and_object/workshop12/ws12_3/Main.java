package d200oop.d211clazz_and_object.workshop12.ws12_3;

import d200oop.d211clazz_and_object.workshop12.ws12_1.Car;

public class Main {
  public static void main(String[] args) {
    Car myLaborghini = new Car();
    myLaborghini.brand = "Lamborghini";
    myLaborghini.wheels = 4;
    myLaborghini.doors = 2;
    myLaborghini.hasHood = true;
    myLaborghini.maxSpeed = 300;

    System.out.println(myLaborghini);
  }
}
