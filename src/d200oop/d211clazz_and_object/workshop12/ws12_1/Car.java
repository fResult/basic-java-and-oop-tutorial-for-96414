package d200oop.d211clazz_and_object.workshop12.ws12_1;

public class Car {
  public String brand;
  public int wheels;
  public int doors;
  public boolean hasHood;
  public int maxSpeed;
}
