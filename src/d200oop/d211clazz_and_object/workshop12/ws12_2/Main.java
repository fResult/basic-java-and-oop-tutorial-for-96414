package d200oop.d211clazz_and_object.workshop12.ws12_2;

import d200oop.d211clazz_and_object.workshop12.ws12_1.Car;

public class Main {
  public static void main(String[] args) {
    Car car1 = new Car();
    Car car2 = new Car();

    System.out.println(car1);
    System.out.println(car2);

    Car car3 = car2;
    System.out.println(car3);
  }
}
