package d200oop.d211clazz_and_object;

public class Main {
  public static void main(String[] args) {
    Student sila = new Student();
    sila.fullName = "Sila Setthakan-anan";
    sila.exam();

    Student ศิลา = sila;
    System.out.println(ศิลา.fullName);

    sila.fullName = "ศิลา เศรษฐกาลอนันต์";
    System.out.println(ศิลา.fullName);

    ศิลา.fullName = "Sila Setthakan-anan";
    System.out.println(sila.fullName);
  }
}
