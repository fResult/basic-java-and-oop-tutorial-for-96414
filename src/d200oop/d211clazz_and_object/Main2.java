package d200oop.d211clazz_and_object;

public class Main2 {
  public static void main(String[] args) {
    Student student = new Student();

    // Call displayStudentInfo method
    student.displayStudentInfo("Sila Setthakan-anan", "999/99", "Sukhumvit",
        "Phrakhanong", "Bangkok", "Thailand");

    System.out.println();

    Address addr = new Address();
    addr.homeNumber = "999/99";
    addr.street = "Sukhumvit";
    addr.district = "Phrakhanong";
    addr.province = "Bangkok";
    addr.country = "Thailand";

    student.displayStudentInfo("Sila Setthakan-anan", addr);

  }
}
