package d200oop.d221method.workshop13.ws13_2;

public class BasicCalculator {
  public int add(int augend, int addend) {
    return augend + addend;
  }

  public int subtract(int minuend, int subtrahend) {
    return minuend - subtrahend;
  }

  public int multiply(int multiplicand, int multiplier) {
    return multiplicand * multiplier;
  }

  public double divide(int dividend, int divisor) {
    return (double) dividend / (double) divisor;
  }
}
