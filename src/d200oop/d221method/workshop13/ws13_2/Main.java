package d200oop.d221method.workshop13.ws13_2;

import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    System.out.print("Input first number => ");
    int firstNumber = sc.nextInt();
    System.out.print("Input second number => ");
    int secondNumber = sc.nextInt();

    BasicCalculator cal = new BasicCalculator();

    System.out.println("The sum of two numbers is " + cal.add(firstNumber, secondNumber));
    System.out.println("The difference of two numbers is " + cal.subtract(firstNumber, secondNumber));
    System.out.println("The product of two numbers is " + cal.multiply(firstNumber, secondNumber));
    System.out.println("The quotient of two numbers is " + cal.divide(firstNumber, secondNumber));
  }
}
