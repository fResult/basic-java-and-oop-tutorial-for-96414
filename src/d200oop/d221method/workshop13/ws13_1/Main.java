package d200oop.d221method.workshop13.ws13_1;

public class Main {
  public static void main(String[] args) {
    Car car = new Car();
    car.forward();

    int wheels = car.getWheels();
    System.out.println("The car has " + wheels + " wheels.");
  }
}
