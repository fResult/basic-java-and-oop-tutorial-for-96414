package d200oop.d221method.workshop13.ws13_1;

public class Car {
  public String brand;
  public int wheels;
  public int doors;
  public boolean hasHood;
  public int maxSpeed;

  public void start() {
    System.out.println("Car is started.");
  }

  public void stop() {
    System.out.println("Car is stopped.");
  }

  public void forward() {
    System.out.println("Car is forwarding.");
  }

  public void backward() {
    System.out.println("Car is backwarding.");
  }

  public int getWheels() {
    return wheels;
  }
}
