package d200oop.d221method;

public class ReturnPresent {
  public static void main(String[] args) {
    SalaryService salaryService = new SalaryService();
    int realSalary = salaryService.calculateSalary(90_000, 10, 36);
    System.out.println("Your real salary is " + realSalary + " THB");
  }
}

class SalaryService {
  public int calculateSalary(int salary, int workdayOtHours, int weekendOtHours) {

    int hourlyWage = salary / 22 / 8;
    int otWage = (int) (hourlyWage * ((workdayOtHours * 1.5) + (weekendOtHours * 2)));
    int realSalary = salary + otWage;

    return realSalary;
  }
}
