package d200oop.d231constructor;

public class Car {

  public int door;
  public int wheels;
  public boolean hasHood;

  // Default Constructor
  public Car() {
    // ไม่มีคำสั่งอยู่ภายในบล็อก Constructor
  }

  // Constructor ที่สร้างขึ้นเอง
  public Car(boolean hasHood) {
    door = 2;
    wheels = 4;
    this.hasHood = hasHood;
  }

  // Constructor ที่สร้างขึ้นเอง
  public Car (int wheels, boolean hasHood) {
    door = 2;
    this.wheels = wheels;
    this.hasHood = hasHood;
  }

}
