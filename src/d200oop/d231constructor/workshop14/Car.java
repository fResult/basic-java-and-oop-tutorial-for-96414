package d200oop.d231constructor.workshop14;

public class Car {
  public String brand;
  public int wheels;
  public int doors;
  public boolean hasHood;
  public int maxSpeed;

  public Car(String brand, int wheels, int doors, boolean hasHood, int maxSpeed) {
    this.brand = brand;
    this.wheels = wheels;
    this.doors = doors;
    this.hasHood = hasHood;
    this.maxSpeed = maxSpeed;
    System.out.println("Object car is initialed with \n brand = " + brand + ", wheels = " + wheels + ", doors = " + doors +
        ", hasHood = " + true + ", maxSpeed = " + maxSpeed);
  }

  public Car() {
    System.out.println("Object car is initialed.");
  }

  public void start() {
    System.out.println("Car is started.");
  }

  public void stop() {
    System.out.println("Car is stopped.");
  }

  public void forward() {
    System.out.println("Car is forwarding.");
  }

  public void backward() {
    System.out.println("Car is backwarding.");
  }

}
