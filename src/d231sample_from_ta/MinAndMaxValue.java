package d231sample_from_ta;

/*
 * Author: Nut
 */
public class MinAndMaxValue {

  public static void main(String[] args) {
    int[] intArray = {22, 56, 78, 44, 91, 32};

    System.out.println("จำนวนที่น้อยสุดคือ : " + findMinData(intArray));
    System.out.println("จำนวนที่มากสุดคือ : " + findMaxData(intArray));
  }

  private static String findMinData(int[] array) {
    int min = array[0];

    for (int i = 1; i < array.length; i++) {
      if (array[i] < min) {
        min = array[i];
      }
    }

    String result = String.valueOf(min);
    return result;
  }

  private static String findMaxData(int[] array) {
    int max = array[0];

    for (int i = 1; i < array.length; i++) {
      if (array[i] > max) {
        max = array[i];
      }
    }

    String result = String.valueOf(max);
    return result;
  }
}
