package d240sample_from_ta;

import java.util.ArrayList;

public class TestStaticValue {

  public void main(String[] args) {
    StaticValue();
  }


  private void StaticValue() {
    ArrayList<TestModel> modelLst = new ArrayList<>();

    for (int i = 0; i < 10; i++) {
      TestModel model = new TestModel();
      TestModel.setValue1(i);
      model.setValue2(i);
      modelLst.add(model);
    }
    for (int i = 0; i < 10; i++) {
      TestModel model = modelLst.get(i);
      int value1 = TestModel.getValue1();
      int value2 = model.getValue2();
      System.out.println("Value 1 : " + value1 + " Value 2 : " + value2);
    }
  }
}
    /* class ExampleStatic {
        private int nonStaticVariable;
        private static int staticVariable;

        public static void staticMethod() {
            nonStaticVariable = 999;// (1)
            staticVariable = 9999;
        }
        public void nonStaticMethod() {
            nonStaticVariable = 999;
            staticVariable = 9999;
        }
        public static void main(String[] args) {
            nonStaticVariable = 999;// (2)
            staticVariable = 9999;
            nonStaticMethod();// (3)
            staticMethod();
        }
    }*/

