package d240sample_from_ta;

public class TestModel {
  private static int value1;
  private int value2;

  public static int getValue1() {
    return value1;
  }

  public static void setValue1(int value1) {
    TestModel.value1 = value1;
  }

  public int getValue2() {
    return value2;
  }

  public void setValue2(int value2) {
    this.value2 = value2;
  }
}
