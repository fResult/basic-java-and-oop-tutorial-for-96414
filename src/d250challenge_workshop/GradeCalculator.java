package d240sample_from_ta;

public class GradeCalculator {

  public String[] calculateGrade(int[] scores) {
    String[] grades = new String[6];

    for (int i = 0; i < scores.length; i++) {
      if (scores[i] > 75) {
        grades[i] = "H";
      } else if (scores[i] >= 60 && scores[i] <= 75) {
        grades[i] = "S";
      } else {
        grades[i] = "U";
      }
    }

    return grades;
  }

}

class Main2 {
  public static void main(String[] args) {

    int[] scores = {72, 56, 78, 44, 91, 32};

    GradeCalculator cal = new GradeCalculator();
    String[] grades = cal.calculateGrade(scores);

    for (int i = 0; i < grades.length; i++) {
      System.out.println("นักเรียนคนที่ "+ (i+1) + " ได้คะแนน " + scores[i] + " เกรดที่ได้คือ " + grades[i]);
    }

  }
}
