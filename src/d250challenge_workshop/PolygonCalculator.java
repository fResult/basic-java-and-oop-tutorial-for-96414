package d240sample_from_ta;

import java.util.Scanner;

public class PolygonCalculator {
  public int findSquareArea(int width) {
    return (int) Math.pow(width, 2);
  }

  public int findTriangleArea(int base, int height) {
    return base * height / 2;
  }
}

class Main {
  public static void main(String[] args) {
    PolygonCalculator polygonCal = new PolygonCalculator();
    Scanner sc = new Scanner(System.in);

    System.out.println("Find Rectangle Area.");
    System.out.print("Input width: ");
    int width = sc.nextInt();


    System.out.println("The area of square is " + polygonCal.findSquareArea(width));
    System.out.println();

    System.out.println("Find Triangle Area.");
    System.out.print("Input base: ");
    int base = sc.nextInt();
    System.out.print("Input height: ");
    int height = sc.nextInt();

    System.out.println("The area of rhombus is " + polygonCal.findTriangleArea(base, height));

  }
}
