package d100basic_java.d140selection.d144switch_case;

public class SwitchCaseBreakAt15Present {
  public static void main(String[] args) {
    // try to change number to 3, 5, 15 or other.
    int number = 3;

    switch (number) {
      case (3):
        System.out.println("The text of 3 is \"THREE\"");
      case (5):
        System.out.println("The text of 5 is \"FIVE\"");
      case (15):
        System.out.println("The text of 15 is \"FIFTEEN\""); break;
      default:
        System.out.println("The number must be only 3 or 5 or 15");
    }

    System.out.println("...End Program...");
  }
}
