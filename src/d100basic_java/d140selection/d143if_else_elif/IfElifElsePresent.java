package d100basic_java.d140selection.d143if_else_elif;

public class IfElifElsePresent {
  public static void main(String[] args) {

    int yourScore = 40;
    int fullScore = 120;
    int percentage = yourScore * 100 / fullScore;

    if (percentage >= 75) {
      System.out.println("Your percentage is " + percentage + " percent. You get Honour");
    } else if (percentage >= 60) {
      System.out.println("Your percentage is " + percentage + " percent. You get Satisfactory");
    } else {
      System.out.println("Your percentage is " + percentage + " percent. You get Unsatisfactory");
    }

    System.out.println("End Program.");
  }
}
