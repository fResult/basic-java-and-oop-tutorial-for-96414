package d100basic_java.d140selection.d145answer_the_workshop;

public class Workshop3_2_1MultiIf {
  public static void main(String[] args) {
    int score = 0;
    String grade = "N/A";
    if (score > 80 && score <= 100) {
      grade = "A";
    }
    if (score >= 70 && score < 80) {
      grade = "B";
    }
    if (score >= 60 && score < 70) {
      grade = "C";
    }
    if (score < 60 && score >= 0) {
      grade = "F";
    }
    System.out.println("Your score is " + score + ". You get grade " + grade);
  }
}
