package d100basic_java.d140selection.d145answer_the_workshop;

import java.util.Scanner;

public class Workshop3_1 {
  public static void main(String... args) {
    Scanner sc = new Scanner(System.in);

    System.out.print("Input score: ");
    int score = sc.nextInt();

    if (score <= 100 || score >= 0) {
      if (score > 80 && score <= 100) {
        System.out.println("Your score is " + score + ". You get grade A");
      } else if (score >= 70) {
        System.out.println("Your score is " + score + ". You get grade B");
      } else if (score >= 60) {
        System.out.println("Your score is " + score + ". You get grade C");
      } else {
        System.out.println("Your score is " + score + ". You get grade F");
      }
    } else {
      System.out.println("ERROR: Input only 1 - 100 scores");
    }
  }
}
