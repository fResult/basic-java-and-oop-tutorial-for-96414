package d100basic_java.d110data_and_variable.d111print.d112data_and_variable;

public class VariableScopePresent {
  static String globalVar = "GLOBAL";

  public static void main(String[] args) {
    String localVar = "LOCAL";

    System.out.println("This is " + globalVar + " variable");
    System.out.println("This is " + localVar + " variable");
  }
}

