package d100basic_java.d110data_and_variable.d111print.d112data_and_variable;

public class DataChangingPresent {
  public static void main(String[] args) {
    double balance = 0.00;
    double withdrawal;

    // เปิดบัญชีด้วยเงินเริ่มต้น 500 บาท
    balance  = balance + 500;

    // ลูกค้าทำรายการขอดูยอดเงินคงเหลือที่ตู้ ATM
    System.out.println("Check balance:");
    System.out.println("Your balance is " + balance + "\n");

    // ลูกค้าถอนเงินออก 300 บาท
    balance = balance - (withdrawal = 300);

    // แสดงยอดเงินคงเหลือ
    System.out.println("Withdrawal:");
    System.out.println("You withdraw " + withdrawal);
    System.out.println("Your balance is " + balance);
  }
}
