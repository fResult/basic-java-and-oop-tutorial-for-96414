package d100basic_java.d131type_conversion;

public class StringToDoubleParsingPresent {
  public static void main(String[] args) {

    String textInput = "42900.00";
    double mbpPrice = Double.parseDouble(textInput);
    double studentDiscount = 4000.00;
    double finalPrice = mbpPrice - studentDiscount;

    System.out.println("The Mac Book Pro's price for student is " + finalPrice);

  }
}
