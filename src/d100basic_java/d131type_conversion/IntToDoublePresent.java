package d100basic_java.d131type_conversion;

public class IntToDoublePresent {
  public static void main(String... args) {

    int gradeInt = 4;
    double gradeDouble = (double) gradeInt;

    System.out.println("The grade is " + gradeDouble);

  }
}
