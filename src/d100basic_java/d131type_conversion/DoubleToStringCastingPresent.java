package d100basic_java.d131type_conversion;

public class DoubleToStringCastingPresent {
  public static void main(String[] args) {
    double price1 = 60.25;
    Double price2 = 65.50;
    Integer price3 = 63;

    String prices = "";
    prices = prices.concat(String.valueOf(price1));
    prices = prices.concat(" | " + price2.toString());
    prices = prices.concat(" | " + price3.toString());

    System.out.println(prices);
  }
}
