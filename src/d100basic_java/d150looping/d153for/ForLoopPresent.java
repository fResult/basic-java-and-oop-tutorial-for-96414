package d100basic_java.d150looping.d153for;

public class ForLoopPresent {
  public static void main(String[] args) {

    for (int number = 1; number <= 10; number = number + 1) {
      System.out.println(number);
    }
    System.out.println("\n...FINISHED...");

  }
}
