package d100basic_java.d150looping.two_level_looping;

public class MultiplicationTablePresent {
  public static void main(String[] args) {

    for (int multiplicand = 2; multiplicand <= 5; multiplicand++) {
      for (int multiplier = 1; multiplier <= 12; multiplier += 1) {

        int result = multiplicand * multiplier;
        System.out.println(multiplicand + " x " + multiplier + " = " + result);

      }
      System.out.println("--------------");
    }

  }
}
