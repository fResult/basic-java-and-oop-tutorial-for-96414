package d100basic_java.d150looping.two_level_looping;

public class Workshop7 {
  public static void main(String[] args) {

    for (int i = 1; i <= 3; i++) {
      for (int j = 1; j <= 3; j++) {
        System.out.print(i * j);
      }
      System.out.println();
    }
  }

}
