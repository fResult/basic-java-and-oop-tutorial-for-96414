package d100basic_java.d150looping.d154foreach;

import java.util.ArrayList;
import java.util.List;

public class ForEachPresent {
  public static void main(String[] args) {
    Student student1 = new Student("Artit", "Lukemai");
    Student student2 = new Student("Chandra", "Nualphong");
    Student student3 = new Student("Angkan", "Daorung");

    List<Student> students = new ArrayList<>();
    students.add(student1);
    students.add(student2);
    students.add(student3);

    for (Student student: students) {
      System.out.println("Hello " + student);
    }

  }
}

