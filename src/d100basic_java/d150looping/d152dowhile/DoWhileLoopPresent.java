package d100basic_java.d150looping.d152dowhile;

public class DoWhileLoopPresent {
  public static void main(String[] args) {

    int number = 0;
    do {
      System.out.print((number++) + " ");
    } while (number < 10);
    System.out.println("\n...FINISHED...");
  }
}
