package d100basic_java.d150looping.d155answer_the_workshop;

public class Workshop7_1 {
  public static void main(String[] args) {
    for (int i = 5; i<=20; i++) {
      if (i%2 == 0) {
        System.out.println(i);
      }
    }
  }
}
