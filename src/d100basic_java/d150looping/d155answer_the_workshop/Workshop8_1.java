package d100basic_java.d150looping.d155answer_the_workshop;

public class Workshop8_1 {
  public static void main(String[] args) {
    for (int i = 1; i <= 3; i++) {
      for (int j = 1; j <= 3; j++) {
        System.out.print(j * i + " ");
      }
      System.out.println();
    }
  }
}
