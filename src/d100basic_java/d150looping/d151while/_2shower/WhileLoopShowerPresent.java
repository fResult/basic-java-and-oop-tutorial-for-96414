package d100basic_java.d150looping.d151while._2shower;

public class WhileLoopShowerPresent {
  public static void main(String[] args) {
    // int[] theGirls = {55, 33, 77, 88, 11, 999, 22};
    int[] theGirls = {55, 33, 77, 88, 99, 11, 22};

    int index = 0;
    boolean isThreeOfKind = (theGirls[index] == 000 || theGirls[index] == 111 || theGirls[index] == 222 ||
        theGirls[index] == 333 || theGirls[index] == 444 || theGirls[index] == 555 ||
        theGirls[index] == 666 || theGirls[index] == 777 || theGirls[index] == 888 || theGirls[index] == 999);

    while (index < theGirls.length) {

      if (isThreeOfKind == true) {
        System.out.println("She is " + theGirls[index] + " Just take a shower");
        break;
      } else if (index == 6) {
        System.out.println("She is " + theGirls[index] + " not a three of kind.");
        System.out.println("I am clean. So, need not to take a shower.");
      } else {
        System.out.println("She is " + theGirls[index] + " not a three of kind.");
      }

      index++;
    }

  }
}
