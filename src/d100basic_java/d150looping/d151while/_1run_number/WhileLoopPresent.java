package d100basic_java.d150looping.d151while._1run_number;

public class WhileLoopPresent {
  public static void main(String[] args) {

    int number = 0;
    while (number < 10) {
      System.out.print((number++) + " ");
    }
    System.out.println("\n...FINISHED...");
  }
}
