package d100basic_java.d150looping.d151while._3registry;

import java.util.Scanner;

public class WhileLoopRegistryPresent {
  public static void main(String[] args) {

    int academicYear = 2562;
    Scanner input = new Scanner(System.in);
    System.out.print("Year " + academicYear + ". Did you passed this subject ?" +
        "\nInput Y when you passed or else Input N: ");
    String isPassed = input.nextLine();

    if (isPassed.equalsIgnoreCase("Y") || isPassed.equalsIgnoreCase("N")) {
      while (academicYear < 9999 && isPassed.equalsIgnoreCase("N")) {
        System.out.println("Register this subject again...");
        academicYear += 1;

        System.out.print("\nYear " + academicYear + ". Did you passed this subject ?" +
            "\nInput Y when you passed or else Input N: ");
        isPassed = input.nextLine();
      }
      System.out.println("Yeah, I haven't to register it");
    } else {
      System.out.println("Input your answer only Y or N");
    }

  }

}
