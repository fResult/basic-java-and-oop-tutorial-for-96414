package d100basic_java.d121input_from_keyboard;

import java.math.BigDecimal;
import java.util.Scanner;

public class KeyboardInputPresent {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    System.out.print("Insert your int: ");
    int yourInt = sc.nextInt();
    System.out.println("Your int is " + yourInt);

    System.out.print("\nInsert your long: ");
    long yourLong = sc.nextLong();
    System.out.println("Your long is " + yourLong);

    System.out.print("\nInsert your double: ");
    double yourDouble = sc.nextDouble();
    System.out.println("Your double is " + yourDouble);

    System.out.print("\nInsert your float: ");
    float yourFloat = sc.nextFloat();
    System.out.println("Your float is " + yourFloat);

    System.out.print("\nInsert your boolean: ");
    boolean yourBoolean = sc.nextBoolean();
    System.out.println("Your boolean is " + yourBoolean);

    System.out.print("\nInsert your String: ");
    String yourString = sc.next();
    System.out.println("Your String is " + yourString);

    System.out.print("\nInsert your BigDecimal: ");
    BigDecimal yourBigDecimal = sc.nextBigDecimal();
    System.out.println("Your BigDeCimal is " + yourBigDecimal);
  }

}
