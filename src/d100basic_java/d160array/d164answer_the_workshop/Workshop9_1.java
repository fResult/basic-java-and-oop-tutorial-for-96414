package d100basic_java.d160array.d164answer_the_workshop;

public class Workshop9_1 {
  public static void main(String[] args) {
    int[] numArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    for (int i = 0; i < numArray.length; i++) {
      System.out.print(numArray[i] + " ");
    }
  }
}
