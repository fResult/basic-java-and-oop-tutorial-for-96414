package d100basic_java.d160array.d163three_dimension;

public class ThreeDimensionArrPresent {
  public static void main(String[] args) {

    int[][][] num;

    num = new int[][][]{
        {
            {1, 2},
            {3, 4}
        },
        {
            {5, 6},
            {7, 8}
        },
        {
            {9, 10},
            {11, 12}
        }
    };

//    for (int layer = 0; layer < 3; layer++) {
//      for (int row = 0; row < 2; row++) {
//        for (int col = 0; col < 2; col++) {
//          if (col == 1) {
//            System.out.print(num[layer][row][col]);
//          } else {
//            System.out.print(num[layer][row][col] + " ");
//          }
//        }
//        System.out.println();
//      }
//      System.out.println("=====");
//    }
    for (int layer = 2; layer >= 0; layer--) {
      for (int row = 1; row >= 0; row--) {
        for (int col = 1; col >= 0; col--) {
          if (col == 0) {
            System.out.print(num[layer][row][col]);
          } else {
            System.out.print(num[layer][row][col] + " ");
          }
        }
        System.out.println();
      }
      System.out.println("=====");
    }
  }
}
