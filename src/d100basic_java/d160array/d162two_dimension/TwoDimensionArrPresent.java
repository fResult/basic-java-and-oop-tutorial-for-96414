package d100basic_java.d160array.d162two_dimension;

public class TwoDimensionArrPresent {
  public static void main(String[] args) {
    String[][] students;
    students = new String[][]{
        {"Weerapat", "Jettarin", "Attasit", "Jiraphorn"},
        {"Paweena", "Janepub", "Assawa", "Teerapong"},
        {"Sukanya", "Anawin", "Annop", "Kongkiat"}
    };

    for (int row = 0; row < 3; row++) {
      for (int col = 0; col < 4; col++) {
        if (col < 3) {
          System.out.print(students[row][col] + ", ");
        } else {
          System.out.print(students[row][col]);
        }
      }
      System.out.println();
    }
  }
}
