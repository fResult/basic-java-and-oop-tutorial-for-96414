package d100basic_java.d160array.d161one_dimension;

public class OneDimensionArrPresent {
  public static void main(String[] args) {
    String[] myPets = new String[5];
    myPets[0] = "Cat";
    myPets[1] = "Dog";
    myPets[2] = "Bird";
    myPets[3] = "Rat";
    myPets[4] = "Cockroach";
    myPets = new String[]{"Cat", "Dog", "Bird", "Rat", "Cockroach"};

    System.out.println("===== Before change Rat to Mouse =====");
    for (int index = 0; index < myPets.length; index++) {
      System.out.print(myPets[index] + " ");
    }

    myPets[3] = "Mouse";

    System.out.println("\n\n===== After change Rat to Mouse =====");
    for (int index = 0; index < myPets.length; index++) {
      System.out.print(myPets[index] + " ");
    }

    String[] myPets2 = {"Cat", "Dog", "Bird", "Rat", "Cockroach"};
  }
}
